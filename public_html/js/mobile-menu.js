'use strict';

function isTouch(){
    return ('ontouchstart' in document.documentElement) ? true : false;
}

function initMobMenu(){
    var buttons = document.querySelectorAll("[data-toggle-menu]");
    var mobMenu = document.querySelector(".mobile-menu"); 
    var overlay = document.querySelector(".overlay-mobile");
    var body = document.getElementsByTagName("body"); 
    var typeEvent = isTouch() ? "touchstart" : "click";
    var timeTransition = 0;
    if(!mobMenu) {
        return;
    }
    if(buttons.length > 0){
        [].forEach.call(buttons, function(element, index){
            element.addEventListener(typeEvent, function(event){
                if(element.classList.contains("is-active")){
                    element.classList.remove("is-active");
                    mobMenu.classList.remove("is-show");
                    overlay.classList.remove("show");
                    body[0].classList.remove("page-overflow");
                    setTimeout(function(){
                        mobMenu.classList.remove("is-open");
                    },timeTransition);
                } else {
                    element.classList.add("is-active");
                    mobMenu.classList.add("is-open");
                    overlay.classList.add("show");
                    body[0].classList.add("page-overflow");
                    setTimeout(function(){
                        mobMenu.classList.add("is-show");
                    },0);
                }
                return false;
            });
        }, false);
    }
};

$(function(){
    initMobMenu();
});