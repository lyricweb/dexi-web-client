


(function () {
  'use strict';
  window.addEventListener('load', function () {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function declOfNum(num, expressions) {  
  var result;
  count = num % 100;
  if (count >= 5 && count <= 20) {
      result = expressions['2'];
  } else {
      count = count % 10;
      if (count == 1) {
          result = expressions['0'];
      } else if (count >= 2 && count <= 4) {
          result = expressions['1'];
      } else {
          result = expressions['2'];
      }
  }
  return result;
};


$(document).ready(function(){
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  var arrBonus = ['бонус', 'бонуса', 'бонусов']

  val = declOfNum(+$('.bonus-order .count-bonus').text(), arrBonus);
  $('.bonus-order .word').text(val);
  $('.bonus-order .count-bonus').on('change',function(){
    val = declOfNum(+$(this).text(), arrBonus);
    $('.bonus-order .word').text(val);
  });
  val = declOfNum(+$('.standart .count-bonus').text(), arrBonus);
  $('.standart .word').text(val);
  $('.standart .count-bonus').on('change',function(){
    val = declOfNum(+$(this).text(), arrBonus);
    $('.standart .word').text(val);
  });
  val = declOfNum(+$('.pay-bonus .pay-count-bonus').text(), arrBonus);
  $('.pay-bonus .word').text(val);
  $('.pay-bonus .pay-count-bonus').on('change',function(){
    val = declOfNum(+$(this).text(), arrBonus);
    $('.pay-bonus .word').text(val);
  });

  val = declOfNum(+$('.count-products').text(), ['товар','товара', 'товаров']);
  $('.basket-count-products .word').text(val);
  $('.count-products').on('change',function(){
    val = declOfNum(+$(this).text(), ['товар','товара', 'товаров']);
    $('.basket-count-products .word').text(val);
  });
  
  (function() {
    var row_relative = parseInt($('.row-relative').innerWidth());
    var helperBlock = $('.helper').width();
    $('.menu > li').mouseenter(function() {
      $('.overlay-submenu').css('display', 'none').css('text-align', 'left').css('display', 'block');
      $('.overlay-submenu .submenu').remove();
      var el = $(this);
      var overlayMenu = $('.overlay-submenu');
      var rt, offsetLeft, elem, wrap;
      wrap = $(this).find('.submenu').clone();
      elem = $(this).find('.submenu');
      offsetLeft = parseInt(elem.offset().left);
      overlayMenu.append(wrap);
      elem.css('opacity', '0');
      wrap.addClass('submenu-fake').css('left', (offsetLeft - helperBlock) + 'px').css('opacity', '1').css('display', 'inline-block').css('position', 'relative');
      var second_point = (parseInt(wrap.width()) + parseInt(wrap.offset().left) - helperBlock);
      if(second_point > row_relative) {
        rt = ($(window).width() - (el.offset().left + el.outerWidth()));
        $('.overlay-submenu').css('text-align', 'right');
        wrap.css({'right': rt, 'left': 'auto'}).css('text-align', 'right');
      } else {
        rt = el.offset().left - (elem.outerWidth() - 5);
        wrap.css('left', (offsetLeft - 45 - helperBlock) + 'px');
      }
      if (!($(this).siblings(".submenu"))) {
        $('.overlay-submenu .submenu').remove();
        $(this).css('display', 'none');
      }
    });
  })();
  function windowSizeHistory() {
    if ($(window).width() <= 768){
      var offsetTop;
      $('.amount-products').unbind('click')
      $('.amount-products').click(function () {
        offsetTop = $(this).offset().top - $(window).scrollTop() - 6
        $(this).parents('.history__info').siblings('.history__add-info').addClass('mobile-show').css({'top': offsetTop + 'px'});
        $('body').addClass('page-overflow')
      });
      $('.add-row-title .amount-products').click(function () {
        $('body').removeClass('page-overflow')
        $(this).parents('.history__add-info').removeClass('mobile-show');
      });
    }
  }
  function windowSizeFilter() {
    if ($(window).width() <= 1199){
      var offsetTop;
      $('.filter-btn').click(function () {
        offsetTop = $(this).offset().top - 155
        $('.filter').css({'top': offsetTop + 'px'});
      });
    }
  }
  $(window).on('load resize', windowSizeHistory);
  $(window).on('load resize', windowSizeFilter);

  $(window).on('load resize', function() {
    var widthMenu = $('.main .col-xl-2').width()
    $('.dropdown-list').css('left', widthMenu + 'px')
  });

  $(window).scroll(function(){
    var sticky = $('.header'),
        scroll = $(window).scrollTop();
  
    if (scroll > 120) {
      $('.main').addClass('head-fix')
      $('.wrap-nav-main').addClass('scroll-menu')
      $('.wrap-nav-main .dropdown-list').addClass('menu-fix')
      sticky.addClass('fixed');
    }
    else if (scroll < 100) {
      $('.main').removeClass('head-fix')
      $('.wrap-nav-main').removeClass('scroll-menu')
      $('.wrap-nav-main .dropdown-list').removeClass('menu-fix')
      sticky.removeClass('fixed');
    }
  });

  var heightMenu = $('.wrap-nav-main').height()
  var heightMainContent = $('.main__content').height() + 120
  var heightHeader = $('.header').height() + 25
  $('.main').css('min-height', heightMenu + 'px')
  if (heightMainContent < heightMenu) {
    $('.wrap-nav-main .dropdown-list').css('height', heightMenu + 'px')
  } else {

    $('.wrap-nav-main .dropdown-list').css('height', heightMainContent + 'px')
    $('.wrap-nav-main .dropdown-list').css('max-height', window.innerHeight - heightHeader + 'px')
  }
  $('.wrap-nav-main .dropdown-list').css('top', heightHeader + 'px')

  $('.menu > li').mouseleave(function() {
      $(this).find('.submenu').css('left', '0');
  });
  if ($('.menu > li').mouseleave() && $('.overlay-submenu').mouseleave()) {
    $('.menu > li').removeClass('active');
  }
  $('.overlay-submenu').mouseleave(function() {
    $('.overlay-submenu .submenu').remove();
    $(this).css('display', 'none').css('text-align', 'left');
  });
  $('.left-menu .nav-main .nav-link').mouseenter(function () {
    $(this).siblings('.dropdown-list').addClass('active')
  });
  $('.left-menu .nav-main .nav-item').mouseleave(function () {
    $(this).find('.dropdown-list').removeClass('active')
  });

  $('.popup__close').click(function () {
    $('.popup').fadeOut();
    $('body').removeClass('page-overflow')
  });
  $('.popup-order').click(function () {
    $('.popup-login').fadeIn()
    $('body').addClass('page-overflow')
  });

  $('.bulk-prices').click(function () {
    $('.popup-bulk-prices').fadeIn()
    $('body').addClass('page-overflow')
  });
  $('.support-form').click(function () {
    $('.popup-support').fadeIn()
    $('body').addClass('page-overflow')
  });

  $('.filter label').click(function(e) {
    $('.show-results').css('top', ($(this).position().top - $('.show-results').height() / 2) + 'px')
  });
  /* $('.filter .range-wrap span').click(function(e) {
    $('.show-results').css('top', ($(this).position().top - $('.show-results').height() / 2) + 'px')
  }) */

  /* $('.filter input').input(function() {
    console.log( "Handler for .change() called." );
  }); */

  // функция подсчета количества товаров в подтверждении заказа

  function countConfirmGoods() {
    var goodsNumber = $('.confirm__product h5 span')
    var amount = $('.confirm__wrap .confirm__row')
    var totalSum = $('.confirm__product .total-sum span')
    var confirmTotalSpan = $('.confirm-total span')
    var confirmTotal = 0
    var counter = 0;

    confirmTotalSpan.each(function() {
      confirmTotal = confirmTotal + parseFloat($(this).text().replace(/\s/g, ''))
    })

    totalSum.text(confirmTotal.toLocaleString('ru'))

    amount.each(function() {
      counter = counter + 1
    })

    goodsNumber.text(counter)
  };
  countConfirmGoods()

  // функция подсчета в корзине
  
  var totalSum = $('.basket-info .total-sum span');

  function countInfoGoods() {
    var sum = 0;
    var goods = 0;

    $('.basket__list .action-product .price-counter').each(function() {
      var pC = parseFloat($(this).text().replace(/\s/g, ''));
      sum = sum + pC
    })

    $('.basket__list .product-wrap').each(function() {
      goods = goods + 1
    })
    var totalGoods = $('.basket-count-products .count-products');
    
    totalSum.text(sum.toLocaleString('ru'))
    totalGoods.text(goods)
  };
  countInfoGoods()

  // функция подсчета количества товаров в истории ЛК

  function countHistoryGoods() {
    var amount = $('.amount-products span')

    amount.each(function() {
      var counter = 0;
      var countOrder = $(this).parents('.history__table').find('.history__add-info').find('.add-row')
      countOrder.each(function() {
        counter = counter + 1
      })
      $(this).text(counter)
    })
  };
  countHistoryGoods()

  var totalSumDetail = $('.product-detail__right .price-counter'); // для увеличивания/уменьшения количества товаров
  var totalSumBasket = $('.basket__list .price-counter'); // собираем все элементы с ценами в корзине
  var unitDetail = parseFloat(totalSumDetail.text().replace(/\s/g, '')); // для увеличивания/уменьшения количества товаров
  var unitBasket = []

  totalSumBasket.each(function() {
    unitBasket.push(parseFloat($(this).text().replace(/\s/g, ''))); // кидаем все найденные цены в массив
  })
  
  $('.plus').click(function () {
    var $input = $(this).parent().find('.current-count')
    
    var newVal = +($input.val()) + 1
    $input.val(newVal)
    var count = parseInt($input.val());
    var el = $(this).parents('.col-xl-3').index();
    var thisTotalSumBasket = $(this).parents('.counter').siblings('.action-product').find('.price-counter')
    
    if ($(this).parents('.counter').find('.current-count').val() > 9) {
      $(this).parents('.counter').find('.placeholder').addClass('d-none')
    }

    thisTotalSumBasket.text((unitBasket[el] * count).toLocaleString('ru'))
    totalSumDetail.text((unitDetail * count).toLocaleString('ru'))

    countInfoGoods()
    return false;
  });

  $('.counter').click(function () {
    event.preventDefault()
  });

  $('.search__input').bind('input', function() {
    if ($(this).val().length > 0) {
      console.log('sdf')
      $('.search__clear').addClass('d-block')
      $('.search__results').removeClass('d-none')
    } else {
      $('.search__results').addClass('d-none')
    }
  })

  $('.search__clear').click(function() {
    $('.search__input').val('')
    $(this).removeClass('d-block')
    $('.search__results').addClass('d-none')
  })

  $('.current-count').bind('input', function(e) {
    var $this = $(this)
    var val = $this.val()
    
    if (val.indexOf('.')) {
      val = parseInt(val)
      $this.val(val)
      e.preventDefault()
    }
    var $input = +($(this).val())
    var el = $(this).parents('.col-xl-3').index();
    var thisTotalSumBasket = $(this).parents('.counter').siblings('.action-product').find('.price-counter')
    if ($(this).val() < 10) {
      $(this).parent().find('.placeholder').removeClass('d-none')
    } else {
      $(this).parent().find('.placeholder').addClass('d-none')
    }

    thisTotalSumBasket.text(($input * unitBasket[el]).toLocaleString('ru'))
    totalSumDetail.text((unitDetail * $input).toLocaleString('ru'))
    countInfoGoods()
  });

  $('.minus').click(function () {
    var $input = $(this).parent().find('.current-count')
    var newQty = +($input.val()) - 1
    $input.val(newQty)
    var count = parseInt($input.val());
    var el = $(this).parents('.col-xl-3').index();
    var thisTotalSumBasket = $(this).parents('.counter').siblings('.action-product').find('.price-counter')
    
    if ($(this).parents('.counter').find('.current-count').val() < 10) {
      $(this).parents('.counter').find('.placeholder').removeClass('d-none')
    }

    if (count < 1) {
      $input.val(1)
      count = 1
    }

    // $input.text(count).change();
    totalSumDetail.text((unitDetail * count).toLocaleString('ru'))
    thisTotalSumBasket.text((unitBasket[el] * count).toLocaleString('ru'))

    countInfoGoods()
    return false;
  });

  $('#star').raty({
    path: '../svg/',
    starOff: 'star-off.svg',
    starOn: 'star-on.svg'
  });

  $('.left-menu .nav-link').mouseenter(function () {
    $('body').addClass('page-overflow')
    $('body').css('padding-right', '17px')
    // $('.overlay').addClass('show')
  });
  $('.left-menu .nav-link').mouseleave(function () {
    $('body').removeClass('page-overflow')
    $('body').css('padding-right', 0)
    // $('.overlay').removeClass('show')
  });
  
  $('.btn-cancel-reg-b').click(function () {
    $('.registration-b').hide()
    $('.sign-b').show()
  });

  $('.amount-products').click(function () {
    if ($(this).parents('.history__info').siblings('.history__add-info').hasClass('show')) {
      $(this).parents('.history__info').siblings('.history__add-info').removeClass('show');
      $(this).removeClass('active')
    } else {
      $(this).parents('.history__info').siblings('.history__add-info').addClass('show');
      $(this).addClass('active')
    }
  });

  $('.from-date').click(function () {
    if ($(this).parents('.top-row').siblings('.add-row').hasClass('show')) {
      $(this).parents('.top-row').siblings('.add-row').removeClass('show');
      $(this).removeClass('active')
    } else {
      $(this).parents('.top-row').siblings('.add-row').addClass('show');
      $(this).addClass('active')
    }
  });

  var phoneMask = document.querySelectorAll('.phone-mask-input');

  var customMask = new Inputmask('+7(999)999-99-99');
  customMask.mask(phoneMask)

  $('.js-base').select2({
    placeholder: 'Имя *',
    allowClear: true
  });
  $('.main-select').select2({
    allowClear: true
  });
  $('.main-slider').slick({
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    slidesToScroll: 1,
    dots: true,
    prevArrow: `<button class="slider-btn left-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.9374 18.5811C14.3279 18.9717 14.9611 18.9717 15.3516 18.5811C15.7422 18.1906 15.7422 17.5574 15.3516 17.1669L9.82877 11.644L15.3516 6.12117C15.7422 5.73065 15.7422 5.09748 15.3516 4.70695C14.9611 4.31643 14.3279 4.31643 13.9374 4.70695L6.99698 11.6474L8.41119 13.0616L8.41455 13.0583L13.9374 18.5811Z"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="slider-btn right-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.0626 18.5811C9.67205 18.9717 9.03889 18.9717 8.64836 18.5811C8.25784 18.1906 8.25784 17.5574 8.64836 17.1669L14.1712 11.644L8.64836 6.12117C8.25784 5.73065 8.25784 5.09748 8.64836 4.70695C9.03889 4.31643 9.67205 4.31643 10.0626 4.70695L17.003 11.6474L15.5888 13.0616L15.5854 13.0583L10.0626 18.5811Z"/>
                  </svg>
                </button>`
  });
  $('.products-slider').slick({
    infinite: false,
    // centerMode: true,
    // useTransform: false,
    // centerPadding: "40px",
    variableWidth: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: `<button class="slider-btn left-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.9374 18.5811C14.3279 18.9717 14.9611 18.9717 15.3516 18.5811C15.7422 18.1906 15.7422 17.5574 15.3516 17.1669L9.82877 11.644L15.3516 6.12117C15.7422 5.73065 15.7422 5.09748 15.3516 4.70695C14.9611 4.31643 14.3279 4.31643 13.9374 4.70695L6.99698 11.6474L8.41119 13.0616L8.41455 13.0583L13.9374 18.5811Z"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="slider-btn right-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.0626 18.5811C9.67205 18.9717 9.03889 18.9717 8.64836 18.5811C8.25784 18.1906 8.25784 17.5574 8.64836 17.1669L14.1712 11.644L8.64836 6.12117C8.25784 5.73065 8.25784 5.09748 8.64836 4.70695C9.03889 4.31643 9.67205 4.31643 10.0626 4.70695L17.003 11.6474L15.5888 13.0616L15.5854 13.0583L10.0626 18.5811Z"/>
                  </svg>
                </button>`,
    responsive: [
      {
        breakpoint: 1730,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1550,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 2,
          variableWidth: false,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          variableWidth: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.additional-slider').slick({
    centerMode: true,
    useTransform: false,
    slidesToShow: 3,
    prevArrow: `<button class="slider-btn left-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.9374 18.5811C14.3279 18.9717 14.9611 18.9717 15.3516 18.5811C15.7422 18.1906 15.7422 17.5574 15.3516 17.1669L9.82877 11.644L15.3516 6.12117C15.7422 5.73065 15.7422 5.09748 15.3516 4.70695C14.9611 4.31643 14.3279 4.31643 13.9374 4.70695L6.99698 11.6474L8.41119 13.0616L8.41455 13.0583L13.9374 18.5811Z"/>
                  </svg>
                </button>`,
    nextArrow: `<button class="slider-btn right-btn">
                  <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.0626 18.5811C9.67205 18.9717 9.03889 18.9717 8.64836 18.5811C8.25784 18.1906 8.25784 17.5574 8.64836 17.1669L14.1712 11.644L8.64836 6.12117C8.25784 5.73065 8.25784 5.09748 8.64836 4.70695C9.03889 4.31643 9.67205 4.31643 10.0626 4.70695L17.003 11.6474L15.5888 13.0616L15.5854 13.0583L10.0626 18.5811Z"/>
                  </svg>
                </button>`,
    responsive: [
      {
        breakpoint: 1730,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1050,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerMode: false
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
  });
  $('.view').click(function() {
    if ($(this).hasClass('view-tile')) {
      $('.view-list').removeClass('active');
      $(this).addClass('active');
      $(this).parents('.sorting').next('.row').removeClass('products-list').addClass('products-tile');
    } else {
      $('.view-tile').removeClass('active');
      $(this).addClass('active');
      $(this).parents('.sorting').next('.row').removeClass('products-tile').addClass('products-list');
    }
  });
  $('.selected-filter .selected-item').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
    } else {
      $('.selected-filter .selected-item').removeClass('active');
      $(this).addClass('active');
    }
  });
  $('.filter__block h4').click(function() {
    $(this).siblings('.input-wrap').slideToggle();
    $(this).toggleClass('active');
  });
  $('.toggle-write-review').click(function() {
    $(this).parent('.review-wrap').siblings('.write-review').slideToggle();
    $(this).toggleClass('active');
  });
  $('.sorting__list span').click(function() {
    if (!($(this).hasClass('decrease')) && !($(this).hasClass('increase'))) {
      $('.sorting__list span').removeClass();
      $(this).addClass('decrease');
    } else if ($(this).hasClass('decrease')) {
      $(this).removeClass('decrease');
      $(this).addClass('increase');
    } else if ($(this).hasClass('increase')) {
      $(this).addClass('decrease');
      $(this).removeClass('increase');
    }
  });
  let res = $('.checked-favorite input:checked').length
  $('.selected-products').html(res)
  
  $('.checked-favorite input').click(function(){
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
  });
  $('.remove-favorite').click(function() {
    $(this).parents('.col-xl-3').remove()
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
    countInfoGoods()
  });
  $('.select-all').click(function(){
    if ($('.select-all input').is(':checked')) {
      $('.checked-favorite input').prop('checked', true);
    } else {
      $('.checked-favorite input').prop('checked', false);
    }
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
  });
  $('.remove-select').click(function() {
    $('.checked-favorite input:checked').parents('.col-xl-3').remove()
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
    countInfoGoods()
  });
  $('.clean-basket').click(function() {
    $('.basket__list .products-list .col-xl-3').remove()
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
    countInfoGoods()
  });
  $('.basket-product-delete').click(function() {
    $(this).parents('.col-xl-3').remove()
    res = $('.checked-favorite input:checked').length
    $('.selected-products').html(res)
    countInfoGoods()
  });

  if ($(window).width() <= '567'){
    $('.footer__column .footer-title').click(function() {
      $(this).toggleClass('active')
      $(this).siblings('.toggle-list').slideToggle()
    })
  }

  $('.tablet-row .search-btn').click(function() {
    $('.search-area').slideToggle()
  })

  $('.tablet-hamburger').click(function() {
    $(this).find('.hamburger').toggleClass('is-active')
    $('.tablet-menu').slideToggle()
  })

  $('.filter-btn').click(function() {
    $(this).toggleClass('active')
    $('.filter').fadeToggle()
  })
  
  $('.location').click(function() {
    $('.popup-city').fadeIn()
  })
  
  if ($(window).width() <= '767') {
    $('.product-detail .nav-tabs').slick({
      slidesToScroll: 1,
      infinite: false,
      variableWidth: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 680,
          settings: {
            slidesToScroll: 1
          }
        }
      ]
    })
  };
  if ($(window).width() <= '1300') {
    $('.personal-wrap .nav-tabs').slick({
      slidesToScroll: 1,
      infinite: false,
      variableWidth: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 680,
          settings: {
            slidesToScroll: 1
          }
        }
      ]
    })
  };
  if ($(window).width() <= '1350') {
    $('.calculated-page .nav-tabs').slick({
      slidesToScroll: 1,
      infinite: false,
      variableWidth: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 680,
          settings: {
            slidesToScroll: 1
          }
        }
      ]
    })
  };
  $('.compare__row').slick({
    slidesToScroll: 1,
    infinite: false,
    slidesToShow: 4,
    variableWidth: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1880,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })
  if ($(window).width() <= '568') {
    $('.compare .product__description').each(function() {
      if ($(this).text().length > 20) {
        $(this).text( $(this).text().substring(0, 20) + '…');
      }
    });
  };

  $('.compare-delete').click(function () {
    $(this).parents('.compare__cell').remove()
    if ($('.compare__row').css('height') == '0px') {
      $('.compare__characteristic').hide()
    }
  })

  $('.show-more').click(function () {
    $('.personal-info').toggleClass('active')
  })

  $('.nav-item a').on('click', function (e) {
    e.preventDefault();
    $('.nav-link').removeClass('active');
    $(this).tab('show');
  });

  $('.add-item').click(function () {
    $('.power-table').append(`<div class="power-table-row">
                                <div class="power-table-cell" style="width: 30px">
                                  <div>
                                      <svg class="delete-item" viewBox="0 0 30 30" width="30" height="30">
                                        <use xlink:href="svg/icons.svg#delete-item" />
                                      </svg>
                                  </div>
                                </div>
                                <div class="power-table-cell">
                                  <div>
                                      <select class="main-select">
                                        <option label="1">PK75-2-11</option>
                                        <option label="2">PK75-2-12</option>
                                        <option label="3">PK75-2-13</option>
                                        <option label="3">PK75-2-14</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="power-table-cell">
                                  <div>
                                      <select class="main-select">
                                        <option label="1">0.35 мм</option>
                                        <option label="2">0.45 мм</option>
                                        <option label="3">0.55 мм</option>
                                        <option label="3">0.65 мм</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="power-table-cell">
                                  <div>
                                      <div class="input-wrap">
                                        <input class="form-control" type="text">
                                        <span>м</span>
                                      </div>
                                  </div>
                                </div>
                                <div class="power-table-cell">
                                  <div>
                                      <div class="input-wrap">
                                        <input class="form-control" type="text">
                                        <span>мA</span>
                                      </div>
                                  </div>
                                </div>
                                <div class="power-table-cell grey-cell">
                                  <div>
                                      <div class="input-wrap">
                                        <input class="form-control" type="text">
                                        <span>дБ</span>
                                      </div>
                                  </div>
                                </div>
                                <div class="power-table-cell grey-cell">
                                  <div>
                                      <div class="input-wrap">
                                        <input class="form-control" type="text">
                                        <span>В</span>
                                      </div>
                                  </div>
                                </div>
                              </div>
                              `);
    $('.main-select').select2({
      allowClear: true
    });
    $('.delete-item').click(function() {
      $(this).parents('.power-table-row').remove()
    })
  });
  $('.delete-item').click(function() {
    $(this).parents('.power-table-row').remove()
  });
  $('.product__description').each(function() {
    if ($(this).text().length > 49) {
      $(this).text( $(this).text().substring(0, 49) + '…');
    }
  });
  $('.info-block .desc').each(function() {
    if ($(this).text().length > 110) {
      $(this).text( $(this).text().substring(0, 110) + '…');
    }
  });

  $('.show-link').click(function() {
    $(this).parents('.address-info').siblings('.map-wrap').fadeIn()
  });
  
  $('.delivery-map-link').click(function() {
    $(this).parents('.tab-pane').find('.delivery-map-wrap').fadeIn()
  });

  $('.map-close').click(function() {
    $('.map-wrap, .delivery-map-wrap').fadeOut()
  });
  
  $('.availability-title').click(function() {
    event.preventDefault()
    $(this).parents('.product__two').find('.availability').addClass('show')
  })
  $('.availability-close').click(function() {
    $(this).parents('.availability').removeClass('show')
  })
});

var $range = $('.js-range-slider'),
    $from = $('.from'),
    $to = $('.to'),
    range,
    min = $range.data('min'),
    max = $range.data('max'),
    from,
    to;

var updateValues = function () {
  $from.prop('value', from);
  $to.prop('value', to);
};

$range.ionRangeSlider({
  onChange: function (data) {
    from = data.from;
    to = data.to;
    updateValues();
  }
});

range = $range.data('ionRangeSlider');
var updateRange = function () {
  range.update({
    from: from,
    to: to
  });
};

$from.on('input', function () {
  from = +$(this).prop("value");
  if (from < min) {
    from = min;
  }
  if (from > to) {
    from = to;
  }
  updateValues();    
  updateRange();
});

$to.on('input', function () {
  to = +$(this).prop('value');
  if (to > max) {
    to = max;
  }
  if (to < from) {
    to = from;
  }
  updateValues();    
  updateRange();
});
