"use strict";

var App = function(){

  function init(){
    svg4everybody();
    yaMapContacts();
    yaMapDelivery();
  }

  function isTouch(){
    return ('ontouchstart' in document.documentElement) ? true : false;
  }

  /*******   yandex карта в контактах    *******/
  function yaMapContacts() {
    var map = document.querySelector('.map');
    // var pMap = document.querySelector('.pickup-map');
    if (!map) {return}
    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=b539e2d3-1424-4e8a-9d72-a12b47ec8988", function(){
      ymaps.ready(function () {
        var myMap = new ymaps.Map('map1', {
          center: [51.666596, 39.199182],
          zoom: 17,
          controls: []
        });
        var myMap2 = new ymaps.Map('map2', {
          center: [51.678398, 39.203710],
          zoom: 17,
          controls: []
        });
        var myMap3 = new ymaps.Map('map3', {
          center: [55.648702, 37.632251],
          zoom: 17,
          controls: []
        });
        var myMap4 = new ymaps.Map('map4', {
          center: [55.150504, 61.412803],
          zoom: 17,
          controls: []
        });
        var myMap5 = new ymaps.Map('map5', {
          center: [45.050475, 38.976678],
          zoom: 17,
          controls: []
        });
        var myMap6 = new ymaps.Map('map6', {
          center: [55.746390, 52.383567],
          zoom: 17,
          controls: []
        });
        var myPlacemark = new ymaps.Placemark([51.666596, 39.199182], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        var myPlacemark2 = new ymaps.Placemark([51.678398, 39.203710], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        var myPlacemark3 = new ymaps.Placemark([55.648702, 37.632251], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        var myPlacemark4 = new ymaps.Placemark([55.150504, 61.412803], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        var myPlacemark5 = new ymaps.Placemark([45.050475, 38.976678], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        var myPlacemark6 = new ymaps.Placemark([55.746390, 52.383567], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        });
        console.log('dsfds')
        /* var myPlacemark7 = new ymaps.Placemark([55.746390, 52.383567], {
          hintContent: 'Адрес'
        }, {
          iconLayout: 'default#image',
          iconImageHref: 'svg/mark.svg',
          iconImageSize: [59, 72],
          iconImageOffset: [-15, -48]
        }); */
        myMap.geoObjects.add(myPlacemark);
        myMap2.geoObjects.add(myPlacemark2);
        myMap3.geoObjects.add(myPlacemark3);
        myMap4.geoObjects.add(myPlacemark4);
        myMap5.geoObjects.add(myPlacemark5);
        myMap6.geoObjects.add(myPlacemark6);
        /* var myMap7 = new ymaps.Map('pickup-map', {
          center: [45.050475, 38.976678],
          zoom: 17,
          controls: []
        });
        myMap7.geoObjects.add(myPlacemark7); */
      });
    });
  }
  
  function yaMapDelivery() {
    var dMap = document.querySelector('.delivery-map');
    if (!dMap) {return}
    loadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=b539e2d3-1424-4e8a-9d72-a12b47ec8988", function(){
      ymaps.ready(function () {
        new ymaps.Map('pickup-map', {
          center: [45.050475, 38.976678],
          zoom: 17,
          controls: []
        });
        new ymaps.Map('delivery-map', {
          center: [45.050475, 38.976678],
          zoom: 17,
          controls: []
        });
      });
    });
  }

  function loadScript(url, callback){
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    if (script.readyState){ 
      script.onreadystatechange = function(){
        if (script.readyState === "loaded" ||
            script.readyState === "complete"){
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = function(){
        callback();
      };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  }
  
  document.addEventListener("DOMContentLoaded", function(){
    init();
  }, false);

  return{
    isTouch: isTouch,
    loadScript: loadScript,
    yaMapContacts: yaMapContacts,
    yaMapDelivery: yaMapDelivery
  };
}();

