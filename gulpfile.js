"use strict";

const gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    imagemin = require('gulp-imagemin'), // img min
    clean = require('gulp-clean'), // clean dir   
    fs = require('fs'),
    spritesmith = require("gulp.spritesmith"), // img sprite
    merge = require('merge-stream'),// merge stream
    sassGlob = require('gulp-sass-glob'),
    sass = require('gulp-sass'),  // sass компилятор
    concat = require('gulp-concat'),  // объединяет файлы в один бандл
    sourcemaps = require('gulp-sourcemaps'),  // создание карты
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    uglify = require('gulp-uglify'), // js сжатие
    rename = require('gulp-rename'),
    svgmin = require('gulp-svgmin'), // svg min
    path = require('path'),
    svgstore = require('gulp-svgstore'), // svg sprite
    imageminOptipng = require('imagemin-optipng'),
    imageminMozjpeg = require('imagemin-mozjpeg');

let paths = {
    scss: './public_html/scss/**/*.scss',
    css: './public_html/css/**/*',
    scripts: './public_html/js/**/*.js',
    build: './public_html/build/**/*',
    html: './public_html/*.html'
};

// watch
gulp.task('watch', function () {
    gulp.watch(paths.scss, { events: ["add", "change"], delay: 500 }, gulp.series('sсss'));
    gulp.watch(paths.css).on('change', gulp.series('css'));
    gulp.watch(paths.scripts).on('change', gulp.series('scripts'));
    gulp.watch(paths.html).on("change", browserSync.reload);
    gulp.watch(paths.build).on("change", browserSync.reload);
});

//svg min
gulp.task('svgmin', function () {
    return gulp.src('./public_html/svg_raw/*.svg')
        .pipe(svgmin(function getOptions(file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            };
        }))
        .pipe(gulp.dest('./public_html/svg/'));
});
//svg
gulp.task('svgstore', function () {
    return gulp
        .src('./public_html/svg_raw/sprite/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            };
        }))
        .pipe(svgstore())
        .pipe(rename("icons.svg"))
        .pipe(gulp.dest('./public_html/svg/'));
});
// js
gulp.task('scripts', function () {
    var pluginMin = gulp.src([
        './public_html/js/svg4everybody.js',
        './public_html/js/jquery-3.4.0.min.js',
        './public_html/js/popper.min.js',
        './public_html/js/bootstrap.js',
        './public_html/js/select2.min.js',
        './public_html/js/fotorama.js',
        './public_html/js/slick.min.js',
        './public_html/js/jquery.mmenu.js',
        './public_html/js/jquery.inputmask.js',
        './public_html/js/mobile-menu.js',
        './public_html/js/ion-rangeslider.js',
        './public_html/js/jquery.raty.js',
        './public_html/js/owl.carousel.js'
    ])
    .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./public_html/build'));

    var initMin =  gulp.src([
        './public_html/js/init.js'
    ])
    .pipe(uglify())
    .pipe(rename("init.min.js"))
    .pipe(gulp.dest('./public_html/build'));

    return merge(pluginMin, initMin);
});
// sass
gulp.task('sсss', function () {
    return gulp.src('./public_html/scss/bootstrap.scss')
        .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
        .pipe(gulp.dest('./public_html/css'));
});
// css 
gulp.task('css', function () {
    var plugins = [
        autoprefixer({ browsers: ['last 4 version'] }),
        require('cssnano')({
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }]
        }),
    ];
    return gulp.src([
            './public_html/css/bootstrap.css',
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('style.min.css'))
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public_html/build'));
});
//clean img dir
gulp.task('clean', function () {
    if (fs.existsSync('./public_html/images')) {
        return gulp.src('./public_html/images/', { read: false })
            .pipe(clean());
    } else {
        fs.mkdirSync('./public_html/images');
        return gulp.src("./public_html/images/");
    }
});
// min img
gulp.task('images', function () {
    return gulp.src('./public_html/images_raw/**/*')
        .pipe(imagemin([
            imageminOptipng({optimizationLevel: 8}),
            imageminMozjpeg({progressive: true, quality: 80})
        ]))
        .pipe(gulp.dest('./public_html/images/'));
});

// sprite img
gulp.task('sprite', function () {
    var spriteData = gulp.src('./public_html/images/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        imgPath: '../images/sprite.png',
        padding: 4,
        retinaSrcFilter: './public_html/images/sprite/*@2x.png',
        retinaImgName: 'spriteX2.png',
        retinaImgPath: '../images/spriteX2.png'
    }));

    var imgStream = spriteData.img
        .pipe(gulp.dest('./public_html/images'));

    var cssStream = spriteData.css
        .pipe(gulp.dest('./public_html/css/'));

    return merge(imgStream, cssStream);
});
// browserSync
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./public_html/"
        }
    });
});


gulp.task('default', gulp.parallel(gulp.series('clean', 'images', 'sсss', 'css', 'watch'), 'svgmin', 'svgstore', 'scripts', 'browser-sync'));